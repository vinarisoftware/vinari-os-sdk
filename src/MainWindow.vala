/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


namespace UI {
    [GtkTemplate (ui = "/org/vinarisoftware/sdk/UI/MainWindow.ui")]
    public class MainWindow : Gtk.ApplicationWindow {
		[GtkChild] private unowned Gtk.Box mainBox;
		private Gtk.Stack windowArea;
		private Gtk.StackSwitcher stackSwitcher;

        public MainWindow (Gtk.Application app) {
            Object (application: app);
        }

        construct{
			this.set_title("Vinari OS SDK - Vinari Software");
			this.set_resizable(false);

			windowArea=new Gtk.Stack();
			windowArea.set_transition_type(Gtk.StackTransitionType.CROSSFADE);

			stackSwitcher=new Gtk.StackSwitcher();
			stackSwitcher.set_stack(windowArea);
			stackSwitcher.set_sensitive(false);

			UI.Views.ProjectView projectViewOBJ=new UI.Views.ProjectView(ref windowArea);
			UI.Views.MainView mainViewOBJ=new UI.Views.MainView(ref windowArea, ref projectViewOBJ);

			windowArea.add_titled(mainViewOBJ, "start", _("Start"));
			windowArea.add_titled(projectViewOBJ, "projectArea", _("Set up the project"));

			mainBox.append(stackSwitcher);
			mainBox.append(windowArea);
        }
    }
}
