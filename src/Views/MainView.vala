/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


namespace UI.Views {
    public class MainView : Gtk.Box {

		private Gtk.Button appButton;
		private Gtk.Button cliButton;
		private Gtk.Button extButton;

		private Gtk.Image appButtonImage;
		private Gtk.Label appButtonLabel;

		private Gtk.Image cliButtonImage;
		private Gtk.Label cliButtonLabel;

		private Gtk.Image extButtonImage;
		private Gtk.Label extButtonLabel;

		private Gtk.Label welcomeLabel;
		private Gtk.Label chooseLabel;
		private Gtk.Label lastProjectLabel;

		private Gtk.Stack mainWindowStack;
		private UI.Views.ProjectView mainWindowProjectView;

		private GLib.Settings mainViewSettings;

        public MainView (ref Gtk.Stack mainStack, ref UI.Views.ProjectView mainProjectView) {
            this.set_orientation(Gtk.Orientation.VERTICAL);
            this.set_spacing(15);
			this.mainWindowStack=mainStack;
			this.mainWindowProjectView=mainProjectView;
        }

		construct{
			// Box used to diplay the welcome message to the developer
			Gtk.Box welcomeBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
			welcomeBox.set_margin_bottom(25);
			welcomeBox.set_margin_top(15);

			// Boxes that are used inside the buttons
			Gtk.Box appButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box cliButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box extButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);

			mainViewSettings=new GLib.Settings("org.vinarisoftware.sdk");

			welcomeLabel=new Gtk.Label("");
			chooseLabel=new Gtk.Label("");

			if(mainViewSettings.get_string("last-created")!=""){
				lastProjectLabel=new Gtk.Label(_("Last project created: ")+mainViewSettings.get_string("last-created"));
			}else{
				lastProjectLabel=new Gtk.Label("");
			}

			welcomeLabel.set_use_markup(true);
			welcomeLabel.set_markup(_("<span size=\"xx-large\">Welcome to Vinari OS SDK</span>"));

			chooseLabel.set_use_markup(true);
			chooseLabel.set_markup(_("<span weight=\"light\">Choose what you want to develop today</span>"));

			appButton=new Gtk.Button();
			appButton.set_has_frame(false);

			cliButton=new Gtk.Button();
			cliButton.set_has_frame(false);

			extButton=new Gtk.Button();
			extButton.set_has_frame(false);

			appButtonLabel=new Gtk.Label(_("Create a new Vala & GTK application for Vinari OS"));
			cliButtonLabel=new Gtk.Label(_("Create a new C++ command line application for Vinari OS"));
			extButtonLabel=new Gtk.Label(_("Create a new JavaScript GNOME extension for Vinari OS"));

			appButtonImage=new Gtk.Image.from_resource("/org/vinarisoftware/sdk/../Assets/Vala.svg");
			cliButtonImage=new Gtk.Image.from_resource("/org/vinarisoftware/sdk/../Assets/C++.svg");
			extButtonImage=new Gtk.Image.from_resource("/org/vinarisoftware/sdk/../Assets/JavaScript.svg");

			appButtonImage.set_pixel_size(80);
			cliButtonImage.set_pixel_size(80);
			extButtonImage.set_pixel_size(80);

			appButtonBox.append(appButtonImage);
			appButtonBox.append(appButtonLabel);

			cliButtonBox.append(cliButtonImage);
			cliButtonBox.append(cliButtonLabel);

			extButtonBox.append(extButtonImage);
			extButtonBox.append(extButtonLabel);

			appButton.set_child(appButtonBox);
			cliButton.set_child(cliButtonBox);
			extButton.set_child(extButtonBox);

			appButtonLabel.set_hexpand(true);
			cliButtonLabel.set_hexpand(true);
			extButtonLabel.set_hexpand(true);

			appButton.clicked.connect(appButtonAction);
			cliButton.clicked.connect(cliButtonAction);
			extButton.clicked.connect(extButtonAction);

			welcomeBox.append(welcomeLabel);
			welcomeBox.append(chooseLabel);
			welcomeBox.append(lastProjectLabel);

			this.append(welcomeBox);
			this.append(appButton);
			this.append(cliButton);
			this.append(extButton);
        }

		private void appButtonAction(){
			this.mainWindowProjectView.reset_projectType();
			this.mainWindowProjectView.set_projectType(1);
			this.mainWindowProjectView.updateToMatchProjectType();
			mainWindowStack.set_visible_child_name("projectArea");
		}

		private void cliButtonAction(){
			this.mainWindowProjectView.reset_projectType();
			this.mainWindowProjectView.set_projectType(2);
			this.mainWindowProjectView.updateToMatchProjectType();
			mainWindowStack.set_visible_child_name("projectArea");
		}

		private void extButtonAction(){
			this.mainWindowProjectView.reset_projectType();
			this.mainWindowProjectView.set_projectType(3);
			this.mainWindowProjectView.updateToMatchProjectType();
			mainWindowStack.set_visible_child_name("projectArea");
		}
    }
}
