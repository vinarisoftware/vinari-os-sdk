/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


namespace UI.Views {
    public class ProjectView : Gtk.Box {

		private Gtk.Label welcomeLabel;
		private Gtk.Label projectTypeLabel;

		private Gtk.Label projectNameLabel;
		private Gtk.Label projectIDLabel;
		private Gtk.Label projectDeveloperLabel;
		private Gtk.Label projectLocationLabel;
		private Gtk.Label projectGitLabel;

		private Gtk.Entry projectNameEntry;
		private Gtk.Entry projectIDEntry;
		private Gtk.Entry projectDeveloperEntry;
		private Gtk.Entry projectLocationEntry;

		private Gtk.Button chooseLocationButton;
		private Gtk.Button confirmCreationButton;
		private Gtk.Button cancelCreationButton;

		private Gtk.LinkButton docsButton;
		private Gtk.Switch projectGitSwitch;

		private Gtk.Stack mainWindowStack;
		private ushort projectType;
		private string projectDirectory;

		private GLib.Settings projectViewSettings;
		private Gtk.FileChooserNative folderSelector;

		private App.Widgets.MessageDialog msgDialog;

		public ProjectView (ref Gtk.Stack mainStack){
			this.set_orientation(Gtk.Orientation.VERTICAL);
			this.set_spacing(10);
			this.mainWindowStack=mainStack;
		}

		public void set_projectType(ushort projectTypeS){
			this.projectType=projectTypeS;
		}

		public void reset_projectType(){
			this.projectType=0;
			this.projectDirectory=projectViewSettings.get_string("project-location");
		}

		public void updateToMatchProjectType(){
			switch(this.projectType){
				case 1:{							// Vala & GTK 4 project
					projectTypeLabel.set_markup(_("<span weight=\"light\">You've chosen to develop a Vala/GTK 4 application.</span>"));
					projectNameEntry.set_placeholder_text(_("My-new-application"));
					projectIDEntry.set_placeholder_text(_("org.organization.application"));
					docsButton.set_label(_("Visit the Vala's documentation website."));
					docsButton.set_uri("https://valadoc.org/gtk4/Gtk.html");
				}
				break;

				case 2:{							// C++ command line project
					projectTypeLabel.set_markup(_("<span weight=\"light\">You've chosen to develop a C++ CLI application.</span>"));
					projectNameEntry.set_placeholder_text(_("My-new-application"));
					projectIDEntry.set_placeholder_text(_("org.organization.application"));
					docsButton.set_label(_("Visit the C++'s documentation website."));
					docsButton.set_uri("https://devdocs.io/cpp/");
				}
				break;

				case 3:{							// JavaScript & GNOME project
					projectTypeLabel.set_markup(_("<span weight=\"light\">You've chosen to develop a GNOME extension.</span>"));
					projectNameEntry.set_placeholder_text(_("extensionName@repository.org"));
					docsButton.set_label(_("Visit the GNOME Extension's documentation website."));
					docsButton.set_uri("https://gjs.guide/extensions/");
				}
				break;
			}
		}

		construct{
			Gtk.Box textBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box projectNameBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box projectIDBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box projectDeveloperBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box projectLocationBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box projectLocationSubBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box actionButtonsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Box projectGitBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

			projectViewSettings=new GLib.Settings("org.vinarisoftware.sdk");

			welcomeLabel=new Gtk.Label("");
			welcomeLabel.set_use_markup(true);
			welcomeLabel.set_markup(_("<span size=\"xx-large\">Time to set up some stuff</span>"));

			projectTypeLabel=new Gtk.Label("");
			projectTypeLabel.set_use_markup(true);

			projectNameLabel=new Gtk.Label(_("Type the name of your project:"));
			projectIDLabel=new Gtk.Label(_("Type the ID of your project:"));
			projectDeveloperLabel=new Gtk.Label(_("Type the name of the developer of your project:"));
			projectLocationLabel=new Gtk.Label(_("Select where to store your project:"));
			projectGitLabel=new Gtk.Label(_("Enable Git in this project."));

			projectNameEntry=new Gtk.Entry();
			projectNameEntry.set_placeholder_text(_("My-new-application"));

			projectIDEntry=new Gtk.Entry();
			projectIDEntry.set_placeholder_text(_("org.organization.application"));

			projectDeveloperEntry=new Gtk.Entry();
			projectDeveloperEntry.set_placeholder_text(_("My organization, or my name"));

			projectLocationEntry=new Gtk.Entry();
			projectLocationEntry.set_hexpand(true);

			if(projectViewSettings.get_string("project-location")!=""){
				this.projectDirectory=projectViewSettings.get_string("project-location");
			}else{
				this.projectDirectory=GLib.Environment.get_home_dir()+"/";
			}

			projectLocationEntry.set_text(this.projectDirectory);

			Gtk.Box browseLocationButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Label browseLocationButtonLabel=new Gtk.Label(_("Browse"));
			Gtk.Image browseLocationButtonImage=new Gtk.Image.from_icon_name("folder-open-symbolic");

			browseLocationButtonBox.append(browseLocationButtonImage);
			browseLocationButtonBox.append(browseLocationButtonLabel);

			chooseLocationButton=new Gtk.Button();
			chooseLocationButton.set_child(browseLocationButtonBox);

			confirmCreationButton=new Gtk.Button.with_label(_("Create project"));
			cancelCreationButton=new Gtk.Button.with_label(_("Cancel"));

			confirmCreationButton.set_hexpand(true);
			cancelCreationButton.set_hexpand(true);

			docsButton=new Gtk.LinkButton.with_label("https://vinarios.me/developer", _("Visit the Vinari OS website."));
			projectGitSwitch=new Gtk.Switch();

			projectGitSwitch.set_active(projectViewSettings.get_boolean("enable-git"));

			textBox.append(welcomeLabel);
			textBox.append(projectTypeLabel);
			textBox.set_margin_top(15);
			textBox.set_margin_bottom(15);

			projectNameBox.append(projectNameLabel);
			projectNameBox.append(projectNameEntry);

			projectIDBox.append(projectIDLabel);
			projectIDBox.append(projectIDEntry);

			projectDeveloperBox.append(projectDeveloperLabel);
			projectDeveloperBox.append(projectDeveloperEntry);

			projectLocationSubBox.append(projectLocationEntry);
			projectLocationSubBox.append(chooseLocationButton);

			projectLocationBox.append(projectLocationLabel);
			projectLocationBox.append(projectLocationSubBox);

			actionButtonsBox.append(cancelCreationButton);
			actionButtonsBox.append(confirmCreationButton);
			actionButtonsBox.set_margin_top(10);
			actionButtonsBox.set_hexpand(true);

			projectGitBox.append(projectGitSwitch);
			projectGitBox.append(projectGitLabel);
			projectGitBox.set_halign(Gtk.Align.CENTER);

			cancelCreationButton.clicked.connect(cancelCreationButtonAction);
			projectNameEntry.changed.connect(projectNameEntryChanged);
			chooseLocationButton.clicked.connect(chooseLocationButtonAction);
			confirmCreationButton.clicked.connect(confirmCreationButtonAction);

			this.append(textBox);
			this.append(projectNameBox);
			this.append(projectIDBox);
			this.append(projectDeveloperBox);
			this.append(projectLocationBox);

			this.append(docsButton);
			this.append(projectGitBox);

			this.append(actionButtonsBox);
		}

		private void projectNameEntryChanged(){
			projectLocationEntry.set_text(this.projectDirectory+projectNameEntry.get_text());
		}

		private void folderSelectorAction(int response){
			if(response!=Gtk.ResponseType.ACCEPT){
				return;
			}else{
				GLib.File folderSelected=folderSelector.get_current_folder();
				this.projectDirectory=folderSelected.get_path()+"/";

				if(projectNameEntry.get_text()!=""){
					projectLocationEntry.set_text(this.projectDirectory+projectNameEntry.get_text());
				}else{
					projectLocationEntry.set_text(this.projectDirectory);
				}
			}
		}

		private void chooseLocationButtonAction(){
			folderSelector=new Gtk.FileChooserNative(_("Select a folder to store the project files"), null, Gtk.FileChooserAction.SELECT_FOLDER, (_("Select")), (_("Cancel")));
			folderSelector.response.connect(folderSelectorAction);
			folderSelector.show();
		}

		private void cancelCreationButtonAction(){
			reset_projectType();
			mainWindowStack.set_visible_child_name("start");

			projectNameEntry.set_text("");
			projectIDEntry.set_text("");
			projectDeveloperEntry.set_text("");
			projectLocationEntry.set_text(GLib.Environment.get_home_dir()+"/");
		}

		private void gitInitRepo(){
			string[] gitInitArgs={"git", "init", this.projectDirectory};
			string[] spawn_env=Environ.get ();
			Pid child_pid;

			try{
				Process.spawn_async ("/", gitInitArgs, spawn_env, SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD, null, out child_pid);
			}catch(SpawnError e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("An error has occurred while initializing your project as a Git repository.\nDespite this, your project has been created successfully."));
				return;
			}
		}

		private void confirmCreationButtonAction(){
			if(projectNameEntry.get_text()=="" || projectIDEntry.get_text()==""){
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("The ID and project name fields must be filled in order to proceed."));
				return;
			}

			this.projectDirectory=projectLocationEntry.get_text();
			Verificator verificator=new Verificator();
			bool projectNameOk=false;
			bool idOk=false;

			switch(this.projectType){
				case 1:{								// Vala & GTK 4 project
					projectNameOk=verificator.checkProjectName(projectNameEntry.get_text());
					idOk=verificator.checkApplicationID(projectIDEntry.get_text());

					if(!projectNameOk || !idOk){
						return;
					}

					App.Projects.GTK gtk=new App.Projects.GTK(projectNameEntry.get_text(), projectIDEntry.get_text(), projectDeveloperEntry.get_text(), this.projectDirectory);
					bool okGTK=gtk.createProject();

					if(okGTK){
						if(projectGitSwitch.get_active()){
							gitInitRepo();
						}

						this.cancelCreationButtonAction();
					}
				}
				break;

				case 2:{								// C++ command line project
					projectNameOk=verificator.checkProjectName(projectNameEntry.get_text());
					idOk=verificator.checkApplicationID(projectIDEntry.get_text());

					if(!projectNameOk || !idOk){
						return;
					}

					App.Projects.CLI cli=new App.Projects.CLI(projectNameEntry.get_text(), projectIDEntry.get_text(), projectDeveloperEntry.get_text(), this.projectDirectory);
					bool okCLI=cli.createProject();
					if(okCLI){
						if(projectGitSwitch.get_active()){
							gitInitRepo();
						}

						this.cancelCreationButtonAction();
					}
				}
				break;

				case 3:{								// JavaScript & GNOME project
					projectNameOk=verificator.checkNameExtension(projectNameEntry.get_text());
					idOk=verificator.checkApplicationID(projectIDEntry.get_text());

					if(!projectNameOk || !idOk){
						return;
					}

					App.Projects.EXT ext=new App.Projects.EXT(projectNameEntry.get_text(), projectIDEntry.get_text(), projectDeveloperEntry.get_text(), this.projectDirectory);
					bool okEXT=ext.createProject();
					if(okEXT){
						if(projectGitSwitch.get_active()){
							gitInitRepo();
						}

						this.cancelCreationButtonAction();
					}
				}
				break;
			}
		}
	}
}
