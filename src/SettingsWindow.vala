/*
    Copyright (c) 2015 - 2025, Vinari Software
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace UI{
	public class SettingsWindow : Gtk.Window {

		private Gtk.Label defaultLocationLabel;
		private Gtk.Label defaultGitStatusLabel;
		private Gtk.Label warningLabel;

		private Gtk.Entry defaultLocationEntry;
		private Gtk.Switch defaultGitStatusSwitch;
		private Gtk.Button selectDefaultLocationButton;

		private Gtk.Button acceptButton;
		private Gtk.Button cancelButton;

		private GLib.Settings settingsWindowSettings;
		private Gtk.FileChooserNative folderSelector;

		public SettingsWindow(Gtk.Window parent){
			this.set_transient_for(parent);
		}

		construct{
			this.set_title(_("Vinari OS SDK - Vinari Software | Preferences"));
			this.set_resizable(false);
			this.set_modal(true);
			this.set_default_size(380, 180);

			settingsWindowSettings=new GLib.Settings("org.vinarisoftware.sdk");

			Gtk.Box mainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
			mainBox.set_margin_top(10);
			mainBox.set_margin_bottom(10);
			mainBox.set_margin_start(10);
			mainBox.set_margin_end(10);

			Gtk.Box defaultLocationBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Box defaultGitStatusBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Box actionButtonsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Separator separatorI=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
			Gtk.Separator separatorII=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);

			defaultLocationLabel=new Gtk.Label(_("Default location for projects:"));
			defaultLocationLabel.set_halign(Gtk.Align.START);

			defaultLocationEntry=new Gtk.Entry();
			defaultLocationEntry.set_hexpand(true);

			if(settingsWindowSettings.get_string("project-location")!=""){
				defaultLocationEntry.set_text(settingsWindowSettings.get_string("project-location"));
			}else{
				defaultLocationEntry.set_text(GLib.Environment.get_home_dir()+"/");
			}

			selectDefaultLocationButton=new Gtk.Button.with_label(_("Browse"));

			defaultLocationBox.append(defaultLocationEntry);
			defaultLocationBox.append(selectDefaultLocationButton);

			defaultGitStatusLabel=new Gtk.Label(_("Enable Git by default:"));

			defaultGitStatusSwitch=new Gtk.Switch();
			defaultGitStatusSwitch.set_hexpand(true);
			defaultGitStatusSwitch.set_halign(Gtk.Align.START);

			defaultGitStatusSwitch.set_active(settingsWindowSettings.get_boolean("enable-git"));
			defaultGitStatusSwitch.set_halign(Gtk.Align.END);

			defaultGitStatusBox.append(defaultGitStatusLabel);
			defaultGitStatusBox.append(defaultGitStatusSwitch);

			acceptButton=new Gtk.Button.with_label(_("Accept"));
			acceptButton.set_hexpand(true);

			cancelButton=new Gtk.Button.with_label(_("Cancel"));
			cancelButton.set_hexpand(true);

			actionButtonsBox.append(cancelButton);
			actionButtonsBox.append(acceptButton);

			warningLabel=new Gtk.Label(_("All the changes made here will take efect once the SDK has\nbeen re-launched."));

			mainBox.append(defaultLocationLabel);
			mainBox.append(defaultLocationBox);
			mainBox.append(defaultGitStatusBox);

			mainBox.append(separatorI);
			mainBox.append(warningLabel);
			mainBox.append(separatorII);
			mainBox.append(actionButtonsBox);

			cancelButton.clicked.connect(cancelButtonAction);
			acceptButton.clicked.connect(acceptButtonAction);
			selectDefaultLocationButton.clicked.connect(selectDefaultLocationButtonAction);

			this.set_child(mainBox);
		}

		private void folderSelectorAction(int response){
			if(response!=Gtk.ResponseType.ACCEPT){
				return;
			}else{
				GLib.File folderSelected=folderSelector.get_current_folder();
				defaultLocationEntry.set_text(folderSelected.get_path().to_string()+"/");
			}
		}

		private void selectDefaultLocationButtonAction(){
			folderSelector=new Gtk.FileChooserNative(_("Select a folder to store the project files"), this, Gtk.FileChooserAction.SELECT_FOLDER, (_("Select")), (_("Cancel")));
			folderSelector.response.connect(folderSelectorAction);
			folderSelector.show();
		}

		private void cancelButtonAction(){
			this.destroy();
		}

		private void acceptButtonAction(){
			settingsWindowSettings.set_string("project-location", defaultLocationEntry.get_text());
			settingsWindowSettings.set_boolean("enable-git", defaultGitStatusSwitch.get_active());
			settingsWindowSettings.apply();
			this.destroy();
		}

		public void run(){
			this.present();
		}
	}
}
