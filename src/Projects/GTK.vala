/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using App.Widgets;

namespace App.Projects{
	public class GTK{

		private string projectID;
		private string projectName;
		private string devName;
		private string projectFolder;
		private string gresourceLocation;

		private App.Widgets.MessageDialog msgDialog;

		public GTK(string projectName, string projectID, string devName, string projectFolder){
			this.projectName=projectName;
			this.projectID=projectID;
			this.devName=devName;
			this.projectFolder=projectFolder;
			this.gresourceLocation=this.projectID.replace(".", "/");
		}

		public bool createProject(){
			bool isOk;

			if(GLib.FileUtils.test(this.projectFolder, GLib.FileTest.IS_DIR)){
				msgDialog=new App.Widgets.MessageDialog(null, WARNING, OK, _("A folder named '")+ projectName +_("' already exists in")+ this.projectFolder +".");
				return false;
			}

			isOk=this.createStructure();
			if(!isOk){
				return false;
			}

			isOk=this.createLicense();
			if(!isOk){
				return false;
			}

			isOk=this.createMesonBuild();
			if(!isOk){
				return false;
			}

			isOk=this.createGlobalization();
			if(!isOk){
				return false;
			}

			isOk=this.createData();
			if(!isOk){
				return false;
			}

			isOk=this.createSource();
			if(!isOk){
				return false;
			}

			if(GLib.FileUtils.test(this.projectFolder, GLib.FileTest.IS_DIR)){
				msgDialog=new App.Widgets.MessageDialog(null, INFO, OK, _("The project '")+ projectName +_("' was created successfully!"));
				return true;
			}else{
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("The project '")+ projectName +_("' could not be created.\nDo you have enough permissions?"));
				return false;
			}
		}

		private void longWrite(string contentToWrite, DataOutputStream outputChannel){
			uint8[] data = contentToWrite.data;
			long written = 0;

			try{
				while (written < data.length) {
					written += outputChannel.write (data[written:data.length]);
				}
			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating some files for the project."));
			}
		}

		private bool createStructure(){
			if(this.projectName=="test"){
				msgDialog=new App.Widgets.MessageDialog(null, WARNING, OK, _("The project name can not be 'test'."));
				return false;
			}

			try{
				File projectFolder=File.new_for_path(this.projectFolder);
				File assetsFolder=File.new_for_path(this.projectFolder+"/Assets/");
				File dataFolder=File.new_for_path(this.projectFolder+"/data/");
				File sourceFolder=File.new_for_path(this.projectFolder+"/src/");
				File widgetsFolder=File.new_for_path(this.projectFolder+"/src/Widgets");
				File uiFolder=File.new_for_path(this.projectFolder+"/src/UI");
				File globalizationFolder=File.new_for_path(this.projectFolder+"/i18n/");

				projectFolder.make_directory();
				assetsFolder.make_directory();
				dataFolder.make_directory();
				sourceFolder.make_directory();
				widgetsFolder.make_directory();
				uiFolder.make_directory();
				globalizationFolder.make_directory();

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's directory structure."));
				return false;
			}
		}

		private bool createLicense(){
			try{
				GLib.DateTime currentTime = new GLib.DateTime.now_local();
				File licenseFile=File.new_for_path(this.projectFolder+"/LICENSE");
				DataOutputStream output=new DataOutputStream(new BufferedOutputStream.sized(licenseFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				output.put_string("Copyright © "+ currentTime.format ("%Y") +", "+devName+"\n\nYour license agreement goes here.\n");

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's LICENSE file."));
				return false;
			}
		}

		private bool createMesonBuild(){
			try{
				File mesonFile=File.new_for_path(this.projectFolder+"/meson.build");
				DataOutputStream output=new DataOutputStream(new BufferedOutputStream.sized(mesonFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				string mesonContents="# Set the project name, version and laguage being used.\nproject('"+ projectID +"', 'vala', 'c', version : '0.0.1')\n\n# Include the translations module.\ni18n = import('i18n')\n# Include the GNOME module.\ngnome = import('gnome')\n\n# Set our translation domain.\nadd_global_arguments('-DGETTEXT_PACKAGE=\"@0@\"'.format (meson.project_name()), language:'c')\n\nposix_dep = meson.get_compiler('vala').find_library('posix')\n\n";
				mesonContents+="project_sources = [\n\t'Main.vala',\n\t'MainWindow.vala',\n\t'Application.vala',\n]\n\nproject_deps = [\n\tdependency('glib-2.0'),\n\tdependency('gobject-2.0'),\n\tdependency('gtk4'),\n\tdependency('gio-2.0'),\n\tposix_dep,\n]\n\nsubdir('src')\nsubdir('i18n')";

				longWrite(mesonContents, output);

				File mesonI18NFile=File.new_for_path(this.projectFolder+"/i18n/meson.build");
				DataOutputStream outputPO=new DataOutputStream(new BufferedOutputStream.sized(mesonI18NFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				string mesonI18NContents="i18n.gettext(meson.project_name(),\n\targs: [\n\t\t'--directory=' + meson.source_root(),\n\t\t'--from-code=UTF-8'\n\t]\n)";
				longWrite(mesonI18NContents, outputPO);

				File mesonSRCFile=File.new_for_path(this.projectFolder+"/src/meson.build");
				DataOutputStream outputSRC=new DataOutputStream(new BufferedOutputStream.sized(mesonSRCFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				string mesonSRCContents="project_sources+=gnome.compile_resources('project-resources', '" + this.projectID + ".gresource.xml', c_name: 'project')\n\nexecutable(\n\tmeson.project_name(),\n\tproject_sources,\n\tvala_args: '--target-glib=2.74',\n\tdependencies: project_deps,\n\tinstall: false,\n)";
				longWrite(mesonSRCContents, outputSRC);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's Meson files."));
				return false;
			}
		}

		private bool createGlobalization(){
			try{
				File POTFile=File.new_for_path(this.projectFolder+"/i18n/POTFILES");
				File linguasFile=File.new_for_path(this.projectFolder+"/i18n/LINGUAS");

				DataOutputStream outputPOT=new DataOutputStream(new BufferedOutputStream.sized(POTFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				outputPOT.put_string("src/Application.vala\nsrc/Main.vala\nsrc/MainWindow.vala\nsrc/UI/MainWindow.ui\n");

				DataOutputStream outputLINGUAS=new DataOutputStream(new BufferedOutputStream.sized(linguasFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				outputLINGUAS.put_string("es\nfr\nde\npt\nit\n");

				File esFile=File.new_for_path(this.projectFolder+"/i18n/es.po");
				File itFile=File.new_for_path(this.projectFolder+"/i18n/it.po");
				File ptFile=File.new_for_path(this.projectFolder+"/i18n/pt.po");
				File frFile=File.new_for_path(this.projectFolder+"/i18n/fr.po");
				File deFile=File.new_for_path(this.projectFolder+"/i18n/de.po");

				var file_stream=esFile.create (FileCreateFlags.NONE);
				file_stream=itFile.create (FileCreateFlags.NONE);
				file_stream=ptFile.create (FileCreateFlags.NONE);
				file_stream=frFile.create (FileCreateFlags.NONE);
				file_stream=deFile.create (FileCreateFlags.NONE);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's translation files."));
				return false;
			}
		}

		private bool createData(){
			try{
				File gschemaFile=File.new_for_path(this.projectFolder+"/data/"+this.projectID+".gschema.xml");
				DataOutputStream outputGSchema=new DataOutputStream(new BufferedOutputStream.sized(gschemaFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				string gschemaContent="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<schemalist gettext-domain="+ this.projectName +">\n\t<schema id="+ this.projectName +" path="+ this.gresourceLocation +">\n\t</schema>\n</schemalist>";
				longWrite(gschemaContent, outputGSchema);


				File desktopFile=File.new_for_path(this.projectFolder+"/data/"+this.projectID+".desktop");
				DataOutputStream outputDesktop=new DataOutputStream(new BufferedOutputStream.sized(desktopFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				string desktopContent="[Desktop Entry]\nType=Application\nName="+this.projectName+"\n";
				desktopContent+="Comment=Describe your application.\nExec="+this.projectID+"\n";
				desktopContent+="Icon="+ this.projectID +"\nTerminal=false\nCategories=GTK;\nKeywords="+ this.projectName +"\nStartupNotify=true\nX-GNOME-UsesNotifications=false\nX-Desktop-File-Install-Version=1.0";
				longWrite(desktopContent, outputDesktop);

				File dataIconsFolder=File.new_for_path(this.projectFolder+"/data/icons");
				dataIconsFolder.make_directory();

				File dataIconsFile=File.new_for_path(this.projectFolder+"/data/icons/README");
				DataOutputStream outputDataIconsFile=new DataOutputStream(new BufferedOutputStream.sized(dataIconsFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				string dataIconsContent="You will need to place icons with the following sizes:\n\n* 32x32\n* 48x48\n* 64x64\n* 128x128\n* 256x256\n* 512x512\n\nThe files can be '*.svg' or '*.png'.";
				longWrite(dataIconsContent, outputDataIconsFile);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating some miscellaneous data."));
				return false;
			}
		}

		private bool createSource(){
			try{
				File applicationFile=File.new_for_path(this.projectFolder+"/src/Application.vala");
				File mainFile=File.new_for_path(this.projectFolder+"/src/Main.vala");
				File mainWindowFile=File.new_for_path(this.projectFolder+"/src/MainWindow.vala");
				File messageDialogFile=File.new_for_path(this.projectFolder+"/src/Widgets/MessageDialog.vala");
				File gresourceFile=File.new_for_path(this.projectFolder+"/src/" + this.projectID + ".gresource.xml");
				File mainWindowUIFile=File.new_for_path(this.projectFolder+"/src/UI/MainWindow.ui");

				string applicationFileContents="public class Application : Gtk.Application {\n";
				applicationFileContents+="\tpublic Application () {\t\tObject (application_id: \"" + this.projectID + "\", flags: ApplicationFlags.FLAGS_NONE);\t}\n\n\tconstruct {\n\t\tActionEntry[] action_entries = {\n\t\t\t{ \"about\", this.on_about_action },\n\t\t\t{ \"preferences\", this.on_preferences_action },\n\t\t\t{ \"quit\", this.quit }\n";
				applicationFileContents+="\t\t};\n\t\tthis.add_action_entries (action_entries, this);\n\t\tthis.set_accels_for_action (\"app.quit\", {\"<primary>q\"});\n\t\tthis.set_accels_for_action (\"app.preferences\", {\"<primary>comma\"});\n\t}\n\n\tpublic override void activate () {\n\t\tbase.activate ();\n\t\tvar win = this.active_window;\n";
				applicationFileContents+="\t\tif (win == null) {\n\t\t\twin = new UI.MainWindow (this);\n\t\t}\n\t\twin.present();\n\t}\n\n\tprivate void on_about_action () {\n\t\tmessage (\"app.about action activated\");\n\t}\n\n\tprivate void on_preferences_action () {\n\t\tmessage (\"app.preferences action activated\");\n\t}\n}\n";

				string mainFileContents="int main (string[] args) {\n\tvar app = new Application ();\n\treturn app.run (args);\n}";

				string mainWindowFileContents="namespace UI {\n\t[GtkTemplate (ui=\"/" + this.gresourceLocation + "/UI/MainWindow.ui\")]\n\tpublic class MainWindow : Gtk.ApplicationWindow {\n\t\t[GtkChild] private unowned Gtk.Box mainBox;\n\n\t\tpublic MainWindow (Gtk.Application app) {\n\t\t\tObject (application: app);\n\t\t}\n\t\tconstruct{\n\t\t\t";
				mainWindowFileContents+="this.mainBox.set_orientation(Gtk.Orientation.HORIZONTAL);\n\t\t}\n\t}\n}";

				string messageDialogContents="namespace App.Widgets{\n\tpublic class MessageDialog{\n\t\tpublic MessageDialog(Gtk.Window? windowParent, Gtk.MessageType messageType, Gtk.ButtonsType buttonsType, string dialogMessage){\n\t\t\tGtk.MessageDialog thisDialog=new Gtk.MessageDialog(windowParent, Gtk.DialogFlags.MODAL, messageType, buttonsType, dialogMessage);\n\t\t\t";
				messageDialogContents+="thisDialog.set_modal(true);\n\t\t\tthisDialog.present();\n\t\t\tthisDialog.response.connect (() => {\n\t\t\t\tthisDialog.destroy();\n\t\t\t});\n\t\t}\n\t}\n}\n";

				string gresourceContents="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<gresources>\n\t<gresource prefix=\"/" + this.gresourceLocation + "\">\n\t\t<file preprocess=\"xml-stripblanks\">UI/MainWindow.ui</file>\n\t</gresource>\n</gresources>";

				string UIContents="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<interface>\n\t<requires lib=\"gtk\" version=\"4.0\"/>\n\t<template class=\"UIMainWindow\" parent=\"GtkApplicationWindow\">\n\t\t<property name=\"default-width\">600</property>\n\t\t<property name=\"default-height\">300</property>\n\t\t<property name=\"titlebar\">\n\t\t\t<object class=\"GtkHeaderBar\" id=\"headerBar\">\n";
				UIContents+="\t\t\t\t<child type=\"end\">\n\t\t\t\t\t<object class=\"GtkMenuButton\">\n\t\t\t\t\t\t<property name=\"primary\">True</property>\n\t\t\t\t\t\t<property name=\"icon-name\">open-menu-symbolic</property>\n\t\t\t\t\t\t<property name=\"tooltip-text\" translatable=\"yes\">Menu</property>\n\t\t\t\t\t\t<property name=\"menu-model\">primary_menu</property>\n\t\t\t\t\t</object>\n";
				UIContents+="\t\t\t\t</child>\n\t\t\t</object>\n\t\t</property>\n\t\t<property name=\"child\">\n\t\t\t<object class=\"GtkBox\" id=\"mainBox\">\n\t\t\t\t<property name=\"visible\">True</property>\n\t\t\t\t<property name=\"margin-start\">10</property>\n\t\t\t\t<property name=\"margin-end\">10</property>\n\t\t\t\t<property name=\"margin-top\">10</property>\n";
				UIContents+="\t\t\t\t<property name=\"margin-bottom\">10</property>\n\t\t\t\t<property name=\"spacing\">35</property>\n\t\t\t</object>\n\t\t</property>\n\t</template>\n\t<menu id=\"primary_menu\">\n\t\t<section>\n\t\t\t<item>\n\t\t\t\t<attribute name=\"label\" translatable=\"yes\">Preferences</attribute>\n";
				UIContents+="\t\t\t\t<attribute name=\"action\">app.preferences</attribute>\n\t\t\t</item>\n\t\t\t<item>\n\t\t\t\t<attribute name=\"label\" translatable=\"yes\">About</attribute>\n\t\t\t\t<attribute name=\"action\">app.about</attribute>\n\t\t\t</item>\n\t\t\t<item>\n\t\t\t\t<attribute name=\"label\" translatable=\"yes\">Quit</attribute>\n";
				UIContents+="\t\t\t\t<attribute name=\"action\">app.quit</attribute>\n\t\t\t</item>\n\t\t</section>\n\t</menu>\n</interface>\n";

				DataOutputStream outputApplication=new DataOutputStream(new BufferedOutputStream.sized(applicationFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputMain=new DataOutputStream(new BufferedOutputStream.sized(mainFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputWindow=new DataOutputStream(new BufferedOutputStream.sized(mainWindowFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputDialog=new DataOutputStream(new BufferedOutputStream.sized(messageDialogFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputGresource=new DataOutputStream(new BufferedOutputStream.sized(gresourceFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputUI=new DataOutputStream(new BufferedOutputStream.sized(mainWindowUIFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				longWrite(applicationFileContents, outputApplication);
				longWrite(mainFileContents, outputMain);
				longWrite(mainWindowFileContents, outputWindow);
				longWrite(messageDialogContents, outputDialog);
				longWrite(gresourceContents, outputGresource);
				longWrite(UIContents, outputUI);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's source files..."));
				return false;
			}
		}
	}
}