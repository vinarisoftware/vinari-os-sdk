/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using App.Widgets;

namespace App.Projects{
	public class CLI{

		private string projectID;
		private string projectName;
		private string devName;
		private string projectFolder;

		private App.Widgets.MessageDialog msgDialog;

		public CLI(string projectNameC, string projectIDC, string devNameC, string projectFolderC){
			this.projectName=projectNameC;
			this.projectID=projectIDC;
			this.devName=devNameC;
			this.projectFolder=projectFolderC;
		}

		public bool createProject(){
			bool isOk;

			if(GLib.FileUtils.test(this.projectFolder, GLib.FileTest.IS_DIR)){
				msgDialog=new App.Widgets.MessageDialog(null, WARNING, OK, _("A folder named '")+ projectName +_("' already exists in")+ this.projectFolder +".");
				return false;
			}

			isOk=this.createStructure();
			if(!isOk){
				return false;
			}

			isOk=this.createLicense();
			if(!isOk){
				return false;
			}

			isOk=this.createMakefile();
			if(!isOk){
				return false;
			}

			isOk=this.createSource();
			if(!isOk){
				return false;
			}

			if(GLib.FileUtils.test(this.projectFolder, GLib.FileTest.IS_DIR)){
				msgDialog=new App.Widgets.MessageDialog(null, INFO, OK, _("The project '")+ projectName +_("' was created successfully!"));
				return true;
			}else{
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("The project '")+ projectName +_("' could not be created.\nDo you have enough permissions?"));
				return false;
			}
		}

		private void longWrite(string contentToWrite, DataOutputStream outputChannel){
			uint8[] data = contentToWrite.data;
			long written = 0;

			try{
				while (written<data.length){
					written+=outputChannel.write (data[written:data.length]);
				}
			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating some files for the project files."));
			}
		}

		private bool createStructure(){
			if(this.projectName=="test"){
				msgDialog=new App.Widgets.MessageDialog(null, WARNING, OK, _("The project name can not be 'test'."));
				return false;
			}

			try{
				File projectFolder=File.new_for_path(this.projectFolder);
				File includeFolder=File.new_for_path(this.projectFolder+"/include/");
				File srcFolder=File.new_for_path(this.projectFolder+"/src/");
				File localeFolder=File.new_for_path(this.projectFolder+"/i18n/");

				projectFolder.make_directory();
				includeFolder.make_directory();
				srcFolder.make_directory();
				localeFolder.make_directory();

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's directory structure."));
				return false;
			}
		}

		private bool createLicense(){
			try{
				GLib.DateTime currentTime = new GLib.DateTime.now_local();
				File licenseFile=File.new_for_path(this.projectFolder+"/LICENSE");
				DataOutputStream output=new DataOutputStream(new BufferedOutputStream.sized(licenseFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				output.put_string("Copyright © "+ currentTime.format ("%Y") +", "+devName+"\n\nYour license agreement goes here.\n");

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's LICENSE file."));
				return false;
			}
		}

		private bool createMakefile(){
			try{
				File makeFile=File.new_for_path(this.projectFolder+"/GNUmakefile");
				DataOutputStream output=new DataOutputStream(new BufferedOutputStream.sized(makeFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				string makeFileContents="OBJ_DIR=obj/\nBUILD_DIR=build/\nINC_DIR=include/\nSRC_DIR=src/\n\nTARGET=$(BUILD_DIR)"+this.projectName+"\n\nOBJS=$(OBJ_DIR)/main.o\n# Add your source files here.\n\nCXXFLAGS= -Wall -I$(INC_DIR) -std=c++17\n\n";
				makeFileContents+="$(TARGET) : $(OBJS)\n\t@mkdir -p $(BUILD_DIR)\n\tg++ $(CXXFLAGS) $(OBJS) -o $(TARGET)\n\n$(OBJ_DIR)%.o : $(SRC_DIR)%.cpp\n\t@mkdir -p $(OBJ_DIR)\n\tg++ -c -MD $(CXXFLAGS) $< -o $@\n\n-include $(OBJ_DIR)/*.d";
				makeFileContents+="\n\n.PHONY : clean\nclean:\n\t@rm -rf $(OBJ_DIR) $(BUILD_DIR)\n\n.PHONY : install\ninstall:\n\t@strip $(TARGET)\n\t@chmod +x $(TARGET)\n\t@cp -rv $(TARGET) /usr/local/bin/\n\n.PHONY : uninstall\nuninstall:\n\t@rm -rfv /usr/local/bin/"+this.projectName;
				makeFileContents+="\n\n.PHONY: create-i18n\ncreate-i18n:\n\t@mkdir -p ./i18n/\n\t@xgettext --keyword=_ --language=C --add-comments --sort-output -o ./i18n/"+this.projectID+".pot ./src/main.cpp\n\t@msginit --no-translator --input=./i18n/"+this.projectID+".pot --locale=es --output=./i18n/es.po\n\t";
				makeFileContents+="@msginit --no-translator --input=./i18n/"+this.projectID+".pot --locale=it --output=./i18n/it.po\n\t@msginit --no-translator --input=./i18n/"+this.projectID+".pot --locale=fr --output=./i18n/fr.po\n\t@msginit --no-translator --input=./i18n/"+this.projectID+".pot --locale=pt --output=./i18n/pt.po\n\n";
				makeFileContents+=".PHONY: update-i18n\nupdate-i18n:\n\t@xgettext --keyword=_ --language=C --add-comments --sort-output -j -o ./i18n/"+this.projectID+".pot ./src/main.cpp\n\t@msgmerge --update --indent ./i18n/es.po ./i18n/"+this.projectID+".pot\n\t";
				makeFileContents+="@msgmerge --indent --update ./i18n/it.po ./i18n/"+this.projectID+".pot\n\t@msgmerge --indent --update ./i18n/fr.po ./i18n/"+this.projectID+".pot\n\t@msgmerge --indent --update ./i18n/pt.po ./i18n/"+this.projectID+".pot";

				longWrite(makeFileContents, output);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the GNUmakefile..."));
				return false;
			}
		}

		private bool createSource(){
			try{
				File mainFile=File.new_for_path(this.projectFolder+"/src/main.cpp");
				DataOutputStream outputMain=new DataOutputStream(new BufferedOutputStream.sized(mainFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				string mainFileContents="#include <iostream>\n#include <unistd.h>\n#include <locale.h>\n#include <stdio.h>\n#include <libintl.h>\n#include <stdlib.h>\n\n#define _(STRING) gettext(STRING)\n#define textDomain \""+this.projectID+"\"\n#define localeDir \"/usr/share/locale/\"\n\n";
				mainFileContents+="int main(int argc, char** argv){\n\tsetlocale(LC_ALL, \"\");\n\tbindtextdomain(textDomain, localeDir);\n\ttextdomain(textDomain);\n\tint option;\n\n\twhile((option=getopt(argc, argv, \"vh\"))!=-1){\n\t\tswitch(option){\n\t\t\tcase 'v':{\n\t\t\t\tprintf(_(\""+this.projectName+ " version 0.1 \\nDeveloped by: "+this.devName+"\\n\"));";
				mainFileContents+="\n\t\t\t}\n\t\t\tbreak;\n\n\t\t\tcase 'h':{\n\t\t\t\t//Print some basic help for your command line application.\n\t\t\t}\n\t\t\tbreak;\n\t\t}\n\t}\n\n\treturn 0;\n}";

				longWrite(mainFileContents, outputMain);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating some miscellaneous data..."));
				return false;
			}
		}
	}
}