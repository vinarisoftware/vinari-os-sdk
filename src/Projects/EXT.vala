/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using App.Widgets;

namespace App.Projects{
	public class EXT{

		private string projectID;
		private string projectName;
		private string devName;
		private string projectFolder;

		private App.Widgets.MessageDialog msgDialog;

		public EXT(string projectName, string projectID, string devName, string projectFolder){
			this.projectName=projectName;
			this.projectID=projectID;
			this.devName=devName;
			this.projectFolder=projectFolder;
		}

		public bool createProject(){
			bool isOk;

			if(GLib.FileUtils.test(this.projectFolder, GLib.FileTest.IS_DIR)){
				msgDialog=new App.Widgets.MessageDialog(null, WARNING, OK, _("A folder named '")+ projectName +_("' already exists in")+ this.projectFolder +"...");
				return false;
			}

			isOk=this.createStructure();
			if(!isOk){
				return false;
			}

			isOk=this.createMakefile();
			if(!isOk){
				return false;
			}

			isOk=this.createSchema();
			if(!isOk){
				return false;
			}

			isOk=this.createLicense();
			if(!isOk){
				return false;
			}

			isOk=this.createLocale();
			if(!isOk){
				return false;
			}

			isOk=this.createSource();
			if(!isOk){
				return false;
			}

			if(GLib.FileUtils.test(this.projectFolder, GLib.FileTest.IS_DIR)){
				msgDialog=new App.Widgets.MessageDialog(null, INFO, OK, _("The project '")+ projectName +_("' was created successfully!"));
				return true;
			}else{
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("The project '")+ projectName +_("' could not be created.\nDo you have enough permissions?"));
				return false;
			}
		}

		private void longWrite(string contentToWrite, DataOutputStream outputChannel){
			uint8[] data = contentToWrite.data;
			long written = 0;

			try{
				while (written < data.length) {
					written += outputChannel.write (data[written:data.length]);
				}
			}catch(Error e){
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating some files for your project."));
			}
		}

		private bool createStructure(){
			if(this.projectName=="test"){
				msgDialog=new App.Widgets.MessageDialog(null, WARNING, OK, _("The project name can not be 'test'."));
				return false;
			}

			try{
				File projectFolder=File.new_for_path(this.projectFolder);
				File localeFolder=File.new_for_path(this.projectFolder+"/locale/");
				File schemasFolder=File.new_for_path(this.projectFolder+"/schemas/");

				projectFolder.make_directory();
				localeFolder.make_directory();
				schemasFolder.make_directory();

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's directory structure."));
				return false;
			}
		}

		private bool createLicense(){
			try{
				GLib.DateTime currentTime = new GLib.DateTime.now_local();
				File licenseFile=File.new_for_path(this.projectFolder+"/LICENSE");
				DataOutputStream output=new DataOutputStream(new BufferedOutputStream.sized(licenseFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				output.put_string("Copyright © "+ currentTime.format ("%Y") +", "+devName+"\n\nYour license agreement goes here.\n\n");

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's LICENSE file."));
				return false;
			}
		}

		private bool createMakefile(){
			try{
				File makeFile=File.new_for_path(this.projectFolder+"/GNUmakefile");
				DataOutputStream output=new DataOutputStream(new BufferedOutputStream.sized(makeFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				string makeFileContents="SCH_DIR=schemas/\nPO_DIR=locale/\nSRC_DIR=.\n\n.PHONY : install\ninstall:\n\t@mkdir -p /home/$(USER)/.local/share/gnome-shell/extensions/"+this.projectName+"\n\t@msgfmt -o $(PO_DIR)es/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)es/LC_MESSAGES/"+this.projectID+".po\n\t";
				makeFileContents+="@msgfmt -o $(PO_DIR)fr/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)fr/LC_MESSAGES/"+this.projectID+".po\n\t@msgfmt -o $(PO_DIR)it/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)es/LC_MESSAGES/"+this.projectID+".po\n\t@msgfmt -o $(PO_DIR)pt/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)pt/LC_MESSAGES/"+this.projectID+".po";
				makeFileContents+="\n\n\t@glib-compile-schemas $(SCH_DIR)\n\n\t@cp -r ./* /home/$(USER)/.local/share/gnome-shell/extensions/"+this.projectName+"\n\n.PHONY : uninstall\nuninstall:\n\t@rm -rfv /home/$(USER)/.local/share/gnome-shell/extensions/"+this.projectID+"\n\n.PHONY: create-i18n\ncreate-i18n:\n\t";
				makeFileContents+="@mkdir -p $(PO_DIR)es/LC_MESSAGES/ \n\t@mkdir -p $(PO_DIR)it/LC_MESSAGES/\n\t@mkdir -p $(PO_DIR)fr/LC_MESSAGES/\n\t@mkdir -p $(PO_DIR)pt/LC_MESSAGES/\n\t@xgettext --keyword=_ --add-comments --sort-output -o $(PO_DIR)"+this.projectID+".pot *.js\n\t@msginit --no-translator --input=$(PO_DIR)"+this.projectID+".pot --locale=es --output=$(PO_DIR)es/LC_MESSAGES/"+this.projectID+".po\n\t";
				makeFileContents+="@msginit --no-translator --input=$(PO_DIR)"+this.projectID+".pot --locale=it --output=$(PO_DIR)it/LC_MESSAGES/"+this.projectID+".po\n\t@msginit --no-translator --input=$(PO_DIR)"+this.projectID+".pot --locale=fr --output=$(PO_DIR)fr/LC_MESSAGES/"+this.projectID+".po\n\t@msginit --no-translator --input=$(PO_DIR)"+this.projectID+".pot --locale=pt --output=$(PO_DIR)pt/LC_MESSAGES/"+this.projectID+".po\n\n";
				makeFileContents+=".PHONY: update-po\nupdate-po:\n\t@xgettext --keyword=_ --language=C --add-comments --sort-output -j -o $(PO_DIR)"+this.projectID+".pot *.js\n\t@msgmerge --update --indent $(PO_DIR)es/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)"+this.projectID+".pot\n\t@msgmerge --update --indent $(PO_DIR)it/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)"+this.projectID+".pot\n\t";
				makeFileContents+="@msgmerge --update --indent $(PO_DIR)fr/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)"+this.projectID+".pot\n\t@msgmerge --update --indent $(PO_DIR)pt/LC_MESSAGES/"+this.projectID+".mo $(PO_DIR)"+this.projectID+".pot";

				longWrite(makeFileContents, output);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the GNUmakefile."));
				return false;
			}
		}

		private bool createSchema(){
			try{
				File schemaFile=File.new_for_path(this.projectFolder+"/schemas/"+this.projectID+".gschema.xml");
				DataOutputStream output=new DataOutputStream(new BufferedOutputStream.sized(schemaFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				string idPath=this.projectID.replace(".", "/");

				string schemaFileContents="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<schemalist>\n\t<schema  path=\"/"+idPath+"/\" id=\""+this.projectID+"\">\n\t\t<key name=\"example\" type=\"i\">\n\t\t\t<default>0</default>\n\t\t\t<summary>This is an example value for the GSchema</summary>\n\t\t</key>\n\t</schema>\n</schemalist>\n";

				longWrite(schemaFileContents, output);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the GSchema file."));
				return false;
			}
		}

		private bool createLocale(){
			try{
				File esFolder=File.new_for_path(this.projectFolder+"/locale/es/");
				File ptFolder=File.new_for_path(this.projectFolder+"/locale/pt/");
				File itFolder=File.new_for_path(this.projectFolder+"/locale/it/");
				File frFolder=File.new_for_path(this.projectFolder+"/locale/fr/");

				esFolder.make_directory();
				ptFolder.make_directory();
				itFolder.make_directory();
				frFolder.make_directory();

				File esFolderLC=File.new_for_path(this.projectFolder+"/locale/es/LC_MESSAGES/");
				File ptFolderLC=File.new_for_path(this.projectFolder+"/locale/pt/LC_MESSAGES/");
				File itFolderLC=File.new_for_path(this.projectFolder+"/locale/it/LC_MESSAGES/");
				File frFolderLC=File.new_for_path(this.projectFolder+"/locale/fr/LC_MESSAGES/");

				esFolderLC.make_directory();
				ptFolderLC.make_directory();
				itFolderLC.make_directory();
				frFolderLC.make_directory();

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the project's translation folders."));
				return false;
			}
		}

		private bool createSource(){
			try{
				File mainFile=File.new_for_path(this.projectFolder+"/extension.js");
				File prefsFile=File.new_for_path(this.projectFolder+"/prefs.js");
				File styleFile=File.new_for_path(this.projectFolder+"/stylesheet.css");
				File jsonFile=File.new_for_path(this.projectFolder+"/metadata.json");

				string mainFileContents="const GETTEXT_DOMAIN = '"+ this.projectID +"';\n\nconst { GObject, St } = imports.gi;\n\nconst ExtensionUtils = imports.misc.extensionUtils;\nconst Main = imports.ui.main;\nconst PanelMenu = imports.ui.panelMenu;\nconst PopupMenu = imports.ui.popupMenu;\n\nconst _ = ExtensionUtils.gettext;\n\nconst Indicator = GObject.registerClass(\nclass Indicator extends PanelMenu.Button {\n\t_init() {\n\t\tsuper._init(0.0, _('Top bar indicator'));\n\n\t\tthis.add_child(new St.Icon({icon_name: 'face-smile-symbolic', style_class: 'system-status-icon'}));";
				mainFileContents+="\n\n\t\tlet item = new PopupMenu.PopupMenuItem(_('Show notification'));\n\t\titem.connect('activate', () => {\n\t\t\tMain.notify(_(\"This a pop-up notification!\"));\n\t\t});\n\t\tthis.menu.addMenuItem(item);\n\t}\n});\n\n";
				mainFileContents+="class Extension {\n\tconstructor(uuid) {\n\t\tthis._uuid = uuid;\n\t\tExtensionUtils.initTranslations(GETTEXT_DOMAIN);\n\t}\n\n\tenable() {\n\t\tthis._indicator = new Indicator();\n\t\tMain.panel.addToStatusArea(this._uuid, this._indicator);\n\t}\n\n\tdisable() {\n\t\tthis._indicator.destroy();\n\t\tthis._indicator = null;\n\t}\n}\n\nfunction init(meta) {\n\treturn new Extension(meta.uuid);\n}";

				string prefsFileContents="'use strict';\n\nconst {Adw, Gio, Gtk} = imports.gi;\nconst ExtensionUtils = imports.misc.extensionUtils;\n\nfunction init() {}\n\nfunction fillPreferencesWindow(window) {\n\tconst settings = ExtensionUtils.getSettings('"+ this.projectID +"');\n\n\t\tconst page = new Adw.PreferencesPage();\n\t\tconst groupTarget = new Adw.PreferencesGroup({\n\t\t\ttitle: \"This is a title\",\n\t\t\tdescription: \"This is a description for the main preferences window\",\n\t\t});\n\t\tpage.add(groupTarget);\n\n\t\t";
				prefsFileContents+="const FirstActionRow = new Adw.ActionRow({\n\t\t\ttitle: \"An Adwaita (Ugh) ActionRow title\",\n\t\t});\n\t\tgroupTarget.add(FirstActionRow);\n\n\t\tconst FirstActionRowAdjustment = new Gtk.Adjustment({\n\t\t\tlower: 1,\n\t\t\tupper: 100,\n\t\t\tstep_increment: 1,\n\t\t});\n\n\t\t";
				prefsFileContents+="const FirstActionRowSpinButton = new Gtk.SpinButton({\n\t\t\tadjustment: FirstActionRowAdjustment,\n\t\t\tnumeric: true,\n\t\t\tvalign: Gtk.Align.CENTER,\n\t\t\thalign: Gtk.Align.END,\n\t\t});\n\n\t\tFirstActionRow.add_suffix(FirstActionRowSpinButton);\n\t\tFirstActionRow.activatable_widget = FirstActionRowSpinButton;\n\n\t\twindow.add(page);\n}";

				string styleFileContent="/*\n* Your CSS classes must be placed in this file\n*/";

				string jsonFileContents="{\n\t\"description\" : \"Brief description of the extension\",\n\t\"name\" : \"Name of the extension\",\n\t\"shell-version\" : [\n\t\t\"43\"\n\t],\n\t\"url\" : \"https://gitrepo.com/repo\",\n\t\"uuid\" : \""+this.projectName+"\",\n\t\"version\" : 1\n}";

				DataOutputStream outputMain=new DataOutputStream(new BufferedOutputStream.sized(mainFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputPrefs=new DataOutputStream(new BufferedOutputStream.sized(prefsFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputStyle=new DataOutputStream(new BufferedOutputStream.sized(styleFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));
				DataOutputStream outputJSON=new DataOutputStream(new BufferedOutputStream.sized(jsonFile.create(FileCreateFlags.REPLACE_DESTINATION), 65536));

				longWrite(prefsFileContents, outputPrefs);
				longWrite(mainFileContents, outputMain);
				longWrite(styleFileContent, outputStyle);
				longWrite(jsonFileContents, outputJSON);

				return true;

			}catch(Error e){
				print("ERROR: %s\n", e.message);
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("There was an unexpected error while creating the source code files."));
				return false;
			}
		}

	}
}