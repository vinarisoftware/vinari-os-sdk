/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using App.Widgets;

public class Verificator{

	private App.Widgets.MessageDialog msgDialog;

	public bool checkProjectName(string nameToVerify){
		bool okProjectName;

		try{
			Regex projectNameRegex=new Regex("^[a-zA-Z.-]+$");											//It does not accept any kind of spaces or numbers.
			okProjectName=projectNameRegex.match(nameToVerify);

			if(!okProjectName){
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("The project name cannot conatin any spaces or special characters."));
				return false;
			}
		}catch(GLib.RegexError e){
			error("Error: %s", e.message);
		}

		return okProjectName;
	}

	public bool checkApplicationID(string idToVerify){
		bool okProjectID;

		try{
			Regex projectIDRegex=new Regex("^[a-zA-Z]{2,}(?:\\.[a-zA-Z]{2,}){2}$");						//It only accepts strings formatted like: org.vinarisoftware.application
			okProjectID=projectIDRegex.match(idToVerify);

			if(!okProjectID){
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("The project ID must follow the format: 'org.mycompany.myapplication'."));
				return false;
			}
		}catch(GLib.RegexError e){
			error("Error: %s", e.message);
		}

		return okProjectID;
	}

	public bool checkNameExtension(string nameToVerifyExt){
		bool okProjectNameExt;

		try{
			Regex projectNameExtRegex=new Regex("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$");	//Only accepts strings formated as an email
			okProjectNameExt=projectNameExtRegex.match(nameToVerifyExt);

			if(!okProjectNameExt){
				msgDialog=new App.Widgets.MessageDialog(null, ERROR, OK, _("The project name must follow the format: 'extension@repository.com'."));
				return false;
			}
		}catch(GLib.RegexError e){
			error("Error: %s", e.message);
		}

		return okProjectNameExt;
	}
}
